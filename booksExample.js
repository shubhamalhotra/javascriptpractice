
function createBook(title, author, price)
{
	var book = {}
	book.title = title;
	book.author = author;
	book.price = price;
	return book;
}
var book1 = createBook("Effective Java", "Joshua Bloch", "1000");
var book2 = createBook("Spring in Action", "Craig Walls", "1200");
var book3 = createBook("Thinking in Java", "Bruce Eckel", "920");
var book4 = createBook("Head First Java", "Kathy Sierra", "1500");
var bookArray = [book1, book2, book3, book4];
